package locators;
import org.openqa.selenium.By;
public interface pagelocators {
public static By SIGNINBTN=By.xpath(".//*[@id='hdr_user_signin']//*[text()='Sign In']");
public static String fname="authiframe";
public static By USRNAME=By.id("id_username");
public static By PASSWD=By.id("id_password");
public static By SIGNIN=By.xpath(".//input[@value='Sign In']");
public static By ONEWAY=By.id("gi_oneway_label");
public static By SRC=By.id("gi_source_st");
public static By SELSRC=By.xpath(".//*[@id='autoSuggestSrcContainer']//span[contains(text(),'Hyderabad')]");
public static By DEST=By.id("gi_destination_st");
public static By SELDEST=By.xpath(".//*[@id='autoSuggestDesContainer']//span[contains(text(),'Delhi')]");
public static By STRTDATE=By.id("start-date");
public static By SELDATE=By.xpath(".//*[@id='jrdp_start-calen_10_30_2016']//span[text()='30']");
public static By NOOFPSNGRS=By.id("pax_link_common");
public static By CHILDRN=By.name("Children");
public static By PAXCLS=By.id("pax_close");
public static By SLTCLS=By.id("gi_class");
public static By SRCHBTN=By.id("gi_search_btn");
public static By FARES=By.xpath(".//*[@id='onwCity']//span[@name='normalfare']");
public static By FLTNAMES=By.xpath(".//*[@id='onwCity']//div[contains(@class,'flName padL20')]");
}
