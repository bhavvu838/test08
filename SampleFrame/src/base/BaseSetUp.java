package base;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeSuite;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;

public class BaseSetUp {
	WebDriver driver;
	public WebDriver getDriver(){
		return driver;
	}
	public void setDriver(WebDriver driver){
		 this.driver=driver;
	}
  @BeforeSuite
  public void beforeSuite() {
	  System.setProperty("webdriver.chrome.driver", "C:/Users/bhavani.dupati/Downloads/selenium-java-3.0.0-beta2/drivers/chromedriver_win32/chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.goibibo.com/");
  }

  @AfterSuite
  public void afterSuite() {
	  driver.quit();
  }

}
